package promduangsee.nattapong.lab3;

public class Permutetion {

	public  static  long  permute(int  n,  int k){
		if (n == 0 || k == 0 || k > n)
		{
			return 0;
		}
		else{
			int x = n - k;
			System.out.println(n + "! = " + fact(n));
			System.out.println("(" + n + "-" + k + ")! = " + fact(x));
			return fact(n)/fact(x);
		}
	}
	
	public  static  long fact(int n){
		if (n == 0){
			return 1;
		}
		else {
			return n*fact(n - 1);
		}
	}
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub

		
		if  (args.length != 2){
			System.err.println("Usage:Permutation <n> <k>");
			System.exit(1);
		}
		int n = Integer.parseInt(args[0]);
		int k = Integer.parseInt(args[1]);
		long result = permute(n,k);
		System.out.println(n + "!/(" + n + "-" + k + ")! = " + result);
	}

}
