package promduangsee.nattapong.lab3;

public class GradeEvaluation {

	public  static  void computeGrade(double  score) {
		if (score >= 0 && score <= 100) {
			if (score >= 80) {
				System.out.println("The grade for score " + score + " is A");
			} else if (score >= 70) {
				System.out.println("The grade for score " + score + " is B");
			} else if (score >= 60) {
				System.out.println("The grade for score " + score + " is C");
			} else if (score >= 50) {
				System.out.println("The grade for score " + score + " is D");
			} else if (score < 50) {
				System.out.println("The grade for score " + score + " is F");
			}
		}
			else {
			System.out.println(score + " is an invalid score. Please enter score in the range 0-100");
		}
	}
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub

		if (args.length != 1){
			System.err.println("Usage:GradeEvaluation <score>");
			System.exit(1);
		}
		double score = Double.parseDouble(args[0]);
		computeGrade(score);
		
	}

}
