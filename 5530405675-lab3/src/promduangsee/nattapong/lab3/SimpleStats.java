package promduangsee.nattapong.lab3;

import  java.util.Arrays;

public class SimpleStats {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		
		int  x = Integer.parseInt(args[0]);
		
		double [] y =  new  double[x];
		
		double  sum = 0.0;
		
				
				System.out.println("For the input GPAs:");
			
			for (int i = 1 ; i <= x ; i++){
				
				System.out.print  (args[i] + " ");
				y[i-1] = Double.parseDouble(args[i]);
				sum += y[i-1];
			}
				
			
			System.out.println("\nStats : ");
			System.out.println("Avg  GPA  is  "  +  sum/x);	
			Arrays.sort(y);
			
			System.out.println("Min  GPA  is  " + y[0]);
			System.out.println("Max  GPA  is  " + y[x-1]);
		
	}

}
